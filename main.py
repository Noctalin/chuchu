import datetime
import itertools
import math
import os
import sys
import time

from pebble import ProcessPool

from Vector import Vector

w, h = 50, 50

MIDDLE = Vector(w / 2, h / 2)
TOP_LEFT = Vector(0, 0)
TOP_RIGHT = Vector(w, 0)
BOTTOM_LEFT = Vector(0, h)
BOTTOM_RIGHT = Vector(w, h)


def radians_2_vector(rad):
    return Vector(math.cos(rad), math.sin(rad))


def vector_2_radians(v: Vector):
    return math.atan2(v.y, v.x)


def vector_2_degrees(v: Vector):
    return math.degrees(vector_2_radians(v))


if __name__ == '__main__':
    from Rail import StraightRail, RailDirection, Bulldozer, LeftCurvedRail, RightCurvedRail

    start_position = MIDDLE
    direction = RailDirection.RIGHT

    # fill container with Rails
    # there is 2^count_of_curved combinations of left or right curved rails in the container
    # math.pow(2, 10)
    combinations = itertools.product([StraightRail, LeftCurvedRail, RightCurvedRail], repeat=22)
    # container = list(combinations[random.randint(0, len(combinations))])

    next_combination = next(combinations)

    counter = 0

    start = time.time()

    bulldozer = None

    try:

        # with ProcessPool() as pool:
            
            # future = pool.map()

        while next_combination is not None:
        # for x in range(0, 100000):

            bulldozer = Bulldozer(start_position, direction, container=list(next_combination), draw=True)

            while bulldozer.has_rails():
                bulldozer.move(size='TINY')

            current_time = datetime.timedelta(seconds=time.time() - start)

            counter += 1

            if counter % 100_000 == 0:
                print(f'[{counter}]')

            if bulldozer.is_circuit_complete():
                print(f'[{counter}] {bulldozer.pretty_history()}')
                with open("full_circuit.txt", "a") as file:
                    file.write(f'[{counter}][{current_time}] {bulldozer.pretty_history()}')

            bulldozer.show(wait_for_input=False)

            next_combination = next(combinations)

    except KeyboardInterrupt:
        sec = time.time() - start
        end = datetime.timedelta(seconds=sec)
        perf = int(counter / sec)

        print(f'\nInterrupted after {counter} combinations')
        print(f'Ran for {end}')
        print(f'Performance: {perf} simulation/sec')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
