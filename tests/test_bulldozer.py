from copy import copy, deepcopy

import pytest

from Rail import RailDirection, StraightRail, CurvedRail, LeftCurvedRail, RightCurvedRail, CircuitCompleteException
from Vector import Vector


@pytest.fixture
def bulldozer():
    """
    Creates Bulldozer instance in the middle of screen
    Pointing in the bottom direction
    """
    from main import MIDDLE
    from Rail import Bulldozer
    start_position = MIDDLE
    direction = RailDirection.BOTTOM
    # fill container with Rails
    container = []
    container += [StraightRail] * 12
    container += [CurvedRail] * 10

    return Bulldozer(start_position, direction, container)


def test_init(bulldozer):
    from main import MIDDLE
    assert bulldozer.start_position == MIDDLE
    assert bulldozer.position == MIDDLE
    assert bulldozer.direction == RailDirection.BOTTOM

    assert len(bulldozer.rail_history) == 0, 'Initial rail history should be empty'
    assert len(bulldozer.rail_container) == 22, 'Initial rail container should contain 22 Rails'


# def test_possible_directions(bulldozer):
#     possible_directions_list = bulldozer.possible_directions()
#
#     assert len(possible_directions_list) == 3
#
#     assert isinstance(possible_directions_list[RailDirection.STRAIGHT], Vector)
#     assert isinstance(possible_directions_list[RailDirection.TURN_LEFT], Vector)
#     assert isinstance(possible_directions_list[RailDirection.TURN_RIGHT], Vector)
#
#     # first element of possible directions should always be the same direction as the bulldozer's current direction
#     assert possible_directions_list[0] == bulldozer.direction


def test_go_straight(bulldozer):
    """
    Test if we are still facing the same direction as before after going straight
    Going straight is always a possible solution so we skip testing if the direction is correct
    """

    bulldozer.rail_container = [StraightRail]

    bulldozer_position = deepcopy(bulldozer.position)
    bulldozer_direction = deepcopy(bulldozer.direction)

    rail_placed = bulldozer.move()

    # are we on a different position after moving
    assert bulldozer.position != bulldozer_position, 'Bulldozer did not move position'

    # are we still facing the same direction
    assert bulldozer.direction == bulldozer_direction, 'Bulldozer direction should not have changed'

    assert len(bulldozer.rail_history) == 1, 'Rail list should contain one rail'

    assert len(bulldozer.rail_container) == 0, 'Rail container should contain no rail'

    assert isinstance(bulldozer.rail_history[0], StraightRail), 'Rail placed is not a StraightRail'

    assert bulldozer_position == rail_placed.position, 'Rail position should be on the previous Bulldozer position'

    assert isinstance(rail_placed, StraightRail), f'Return object is not a StraightRail, ({type(rail_placed).__name__})'


def test_turn_left(bulldozer):
    """
    Test if we turned left from current position and direction
    """
    bulldozer.rail_container = [LeftCurvedRail]

    bulldozer_position = deepcopy(bulldozer.position)
    bulldozer_direction = deepcopy(bulldozer.direction)

    rail_placed = bulldozer.move()

    # are we on a different position after moving
    assert bulldozer.position != bulldozer_position, 'Bulldozer did not move position'

    # are we still facing the same direction
    assert bulldozer.direction != bulldozer_direction, 'Bulldozer direction did not change'

    assert len(bulldozer.rail_history) == 1, 'Rail list should contain one rail'

    assert len(bulldozer.rail_container) == 0, 'Rail container should contain no rail'

    assert isinstance(bulldozer.rail_history[0], LeftCurvedRail), 'Rail placed is not a LeftCurvedRail'

    assert bulldozer_position == rail_placed.position, 'Rail position should be on the previous Bulldozer position'

    assert isinstance(rail_placed, LeftCurvedRail), f'Return object is not a LeftCurvedRail, ({type(rail_placed).__name__})'


def test_turn_right(bulldozer):
    """
    Test if we turned right from current position and direction
    """
    bulldozer.rail_container = [RightCurvedRail]

    bulldozer_position = deepcopy(bulldozer.position)
    bulldozer_direction = deepcopy(bulldozer.direction)

    rail_placed = bulldozer.move()

    # are we on a different position after moving
    assert bulldozer.position != bulldozer_position, 'Bulldozer did not move position'

    # are we still facing the same direction
    assert bulldozer.direction != bulldozer_direction, 'Bulldozer direction did not change'

    assert len(bulldozer.rail_history) == 1, 'Rail list should contain one rail'

    assert len(bulldozer.rail_container) == 0, 'Rail container should contain no rail'

    assert isinstance(bulldozer.rail_history[0], RightCurvedRail), 'Rail placed is not a RightCurvedRail'

    assert bulldozer_position == rail_placed.position, 'Rail position should be on the previous Bulldozer position'

    assert isinstance(rail_placed, RightCurvedRail), f'Return object is not a RightCurvedRail, ({type(rail_placed).__name__})'


def test_undo(bulldozer):
    """
    Test if we undo a rail that we get back to the previous one
    """

    bulldozer.rail_container = [StraightRail]

    bulldozer.move()

    bulldozer_position = deepcopy(bulldozer.position)
    bulldozer_rail_container = deepcopy(bulldozer.rail_container)
    bulldozer_rail_history = deepcopy(bulldozer.rail_history)

    undone_rail = bulldozer.undo()

    assert len(bulldozer.rail_container) == len(bulldozer_rail_container) + 1, 'Rail container should contain one more'
    assert len(bulldozer.rail_history) == len(bulldozer_rail_history) - 1, 'Rail history should be one less'

    # test if bulldozer is at position of previous rail

    assert bulldozer.position != bulldozer_position, 'Bulldozer did not move'
    assert bulldozer.position == undone_rail.position, 'Bulldozer did not move back to previous Rail'


def test_is_circuit_complete(bulldozer):
    """
    Test if a complete circle counts as a complete circuit
    Test if a complete straight line of rails does not count as complete circuit
    """
    bulldozer.rail_container = [LeftCurvedRail] * 8  # for a full circle of SMALL sized curves

    while bulldozer.has_rails():
        bulldozer.move(size='SMALL')

    assert bulldozer.is_circuit_complete(), f'Bulldozer should be on starting position {bulldozer.start_position} after completing a circuit, was {bulldozer.position}'

    with pytest.raises(CircuitCompleteException):
        bulldozer.move(size='SMALL')


def test_is_circuit_not_complete(bulldozer):
    bulldozer.rail_container = [StraightRail] * 10

    while bulldozer.has_rails():
        bulldozer.move(size='SMALL')

    assert not bulldozer.is_circuit_complete(), f'Bulldozer should not be on starting position {bulldozer.start_position} after completing a circuit, was {bulldozer.position}'
