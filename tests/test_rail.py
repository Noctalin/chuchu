import pytest

from Rail import RailDirection


@pytest.fixture
def rail():
    """
    Creates Rail instance in the middle of screen
    Medium size
    Pointing in the bottom direction
    """
    from main import MIDDLE
    from Rail import Rail
    start_position = MIDDLE
    direction = RailDirection.BOTTOM
    size = Rail.TINY
    return Rail(start_position, direction, size)


def straight_rail():
    """
    Creates StraightRail instance in the middle of screen
    Medium size
    Pointing in the bottom direction
    """
    from main import MIDDLE
    from Rail import StraightRail
    start_position = MIDDLE
    direction = RailDirection.BOTTOM
    size = StraightRail.MEDIUM
    return StraightRail(start_position, direction, size)


def curved_rail():
    """
    Creates CurvedRail instance in the middle of screen
    Medium size
    Pointing in the bottom direction
    """
    from main import MIDDLE
    from Rail import CurvedRail
    start_position = MIDDLE
    direction = RailDirection.BOTTOM
    size = CurvedRail.MEDIUM
    return CurvedRail(start_position, direction, size)
