class Vector:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def as_tuple(self):
        return self.x, self.y

    def __eq__(self, o) -> bool:
        return self.x == o.x and self.y == o.y

    def __ne__(self, o) -> bool:
        return self.x != o.x or self.y != o.y

    def __add__(self, o):
        return Vector(self.x + o.x, self.y + o.y)

    def __sub__(self, o):
        return Vector(self.x - o.x, self.y - o.y)

    def __truediv__(self, o):
        return Vector(self.x / o, self.y / o)

    def __mul__(self, o):
        return Vector(self.x * o, self.y * o)

    def __rmul__(self, o):
        return Vector(self.x * o, self.y * o)
