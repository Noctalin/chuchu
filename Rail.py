import math
import sys

from Vector import Vector
from main import MIDDLE, radians_2_vector, w, h


class RailDirection:
    TOP = math.pi / 2
    LEFT = math.pi
    BOTTOM = 3 * math.pi / 2
    RIGHT = 2 * math.pi

    TOP_LEFT = Vector(-1, 1)
    BOTTOM_LEFT = Vector(-1, -1)
    BOTTOM_RIGHT = Vector(1, -1)
    TOP_RIGHT = Vector(1, 1)

    # these constants are to be used together with possible directions from bulldozer
    STRAIGHT = 0
    TURN_LEFT = 1
    TURN_RIGHT = 2


class Rail:

    TINY = 1

    size = TINY
    position = None

    def get_position(self):
        return self.position

    def get_size(self):
        return self.size

    @staticmethod
    def get_possible_sizes():
        return ['TINY']


class StraightRail(Rail):

    TINY = 1
    SMALL = 2
    MEDIUM = 3
    LONG = 4
    BRIDGE_LONG = 4

    position: Vector

    def __init__(self, position, size):

        # self.axes = axes
        self.position = position

        if size not in self.get_possible_sizes():
            raise Exception(f'{size} is not a valid size')

        self.size = getattr(self, size)

    @staticmethod
    def get_possible_sizes():
        return [
            'TINY',
            'SMALL',
            'MEDIUM',
            'LONG',
            'BRIDGE_LONG'
        ]


class CurvedRail(Rail):

    # represented in radians
    TINY = math.pi / 8.0
    SMALL = math.pi / 4.0  # need 2 small to make a 45% turn

    position: Vector

    def __init__(self, position, size):

        self.position = position

        if size not in self.get_possible_sizes():
            raise Exception(f'{size} is not a valid size')

        self.size = getattr(self, size)

    @staticmethod
    def get_possible_sizes():
        return [
            'TINY',
            'SMALL'
        ]


class LeftCurvedRail(CurvedRail):
    pass


class RightCurvedRail(CurvedRail):
    pass


class CircuitCompleteException(Exception):
    pass


class Bulldozer:

    def __init__(self, start_position=MIDDLE, direction=RailDirection.TOP, container=None, draw=False):

        if container is None:
            container = []

        self.start_position = start_position
        self.position = start_position
        self.completed = False
        self.direction = direction
        self.rail_container = container  # list of available rails as class types not instances!!!!
        self.rail_count = len(container)
        self.rail_history = []  # list to keep track of all placed rails

        # setting if matplotlib is supposed to show dialog with the figure
        self.draw = draw

        if self.draw:
            import matplotlib
            import tkinter
            import matplotlib.pyplot as plt
            matplotlib.use('TkAgg')
            self.plotly = plt
            self.axes = self.plotly.axes()
            self.plotly.xlim(0, w)
            self.plotly.ylim(0, h)

    def _place_rail(self, size: str):
        """
        Pop the next rail from container, instantiate it with current position and append it to history
        """

        rail_cls = None
        rail = None
        try:
            rail_cls = self.rail_container.pop(len(self.rail_container)-1)

            if size is None or size.upper() not in rail_cls.get_possible_sizes():
                size = 'TINY'

            if not issubclass(rail_cls, Rail):
                raise Exception(f'Rail Class is invalid ({rail_cls.__name__})')

            rail = rail_cls(self.position, size)

            # add the popped Rail from container as instantiated Rail object in history
            self.rail_history.append(rail)

            return rail
        except Exception as e:
            if rail is not None and rail is self.rail_history[len(self.rail_history)-1]:
                self.rail_history.pop()
                self.rail_container.append(rail.__class__)
            elif rail is None and rail_cls is not None:
                self.rail_container.append(rail.__class__)

            print(f'[ERROR] Could not place rail: {e}', file=sys.stderr)
            return None

    def _draw_rail(self, delta_position):
        """
        Draw rail from current bulldozer position
        """
        if not self.draw:
            return None

        c = self.plotly.Circle((self.position + delta_position).as_tuple(), 0.15, color='r')
        self.axes.add_artist(c)

        return self.axes.arrow(self.position.x,
                                self.position.y,
                                delta_position.x,
                                delta_position.y,
                                # head_width=1.5, head_length=1.5,
                                fc='k', ec='k')

    def has_rails(self):
        return len(self.rail_container) > 0

    def move(self, size=None):

        if self.completed:
            raise CircuitCompleteException('Circuit is already completed, no need to move anymore')

        if not self.has_rails():
            raise Exception('No more Rails left')

        try:

            rail = self._place_rail(size)

            if rail is None:
                return None

            self.drive_to_rail_end(rail)

            self.completed = self.is_circuit_complete()

            return rail

        except Exception as e:
            self.undo()

    def get_end_position(self, size):

        delta_position = radians_2_vector(self.direction) * size
        return delta_position

    def get_distance(self, size):

        start_position = self.position
        end_position = self.get_end_position(size)

        x = math.pow(abs(end_position.x) - abs(start_position.x), 2)
        y = math.pow(abs(end_position.y) - abs(start_position.y), 2)

        return math.sqrt(x + y)

    def drive_to_rail_end(self, rail):
        # position bulldozer according to curved rail
        if isinstance(rail, LeftCurvedRail):
            self.direction += rail.get_size()
        elif isinstance(rail, RightCurvedRail):
            self.direction -= rail.get_size()

        # position ourselves in direction with a length of size
        delta_position = self.get_end_position(rail.get_size())

        # TODO draw them at the end not during loop for rail placement
        self._draw_rail(delta_position)

        # save the new bulldozer position to the tip of the rail
        self.position += delta_position

    def undo(self):
        """
        Removes the last placed rail from history and reinserts it into the container
        Moves back the Bulldozer to the  undone rail position
        Returns the rail undone
        """
        rail = self.rail_history.pop(len(self.rail_history) - 1)
        self.rail_container.append(type(rail))

        self.position = rail.position

        return rail

    def possible_directions(self, curved_size=1):
        """
        Get next possible directions.
        There is always 3 possible directions where the rail can continue.
        If curved_size is passed it gives the 3 possible directions divided by the size.
        The first element of the returned array is always a straight rail, the other 2 are always curved.
        Second element is always a left going curve.
        Third element is always a right going curve.
        """

        direction = self.direction

        if direction == RailDirection.TOP:
            return [
                RailDirection.TOP,  # first one is always the straight rail
                RailDirection.TOP_LEFT / curved_size,  # second one is always curved rail
                RailDirection.TOP_RIGHT / curved_size,  # third one is always curved rail
            ]
        elif direction == RailDirection.LEFT:
            return [
                RailDirection.LEFT,
                RailDirection.BOTTOM_LEFT / curved_size,
                RailDirection.TOP_LEFT / curved_size,
            ]
        elif direction == RailDirection.BOTTOM:
            return [
                RailDirection.BOTTOM,
                RailDirection.BOTTOM_RIGHT / curved_size,
                RailDirection.BOTTOM_LEFT / curved_size,
            ]
        elif direction == RailDirection.RIGHT:
            return [
                RailDirection.RIGHT,
                RailDirection.TOP_RIGHT / curved_size,
                RailDirection.BOTTOM_RIGHT / curved_size,
            ]

        elif direction == RailDirection.TOP_LEFT:
            return [
                RailDirection.TOP_LEFT,
                RailDirection.LEFT / curved_size,
                RailDirection.TOP / curved_size,
            ]
        elif direction == RailDirection.BOTTOM_LEFT:
            return [
                RailDirection.BOTTOM_LEFT,
                RailDirection.BOTTOM / curved_size,
                RailDirection.LEFT / curved_size,
            ]
        elif direction == RailDirection.BOTTOM_RIGHT:
            return [
                RailDirection.BOTTOM_RIGHT,
                RailDirection.RIGHT / curved_size,
                RailDirection.BOTTOM / curved_size,
            ]
        elif direction == RailDirection.TOP_RIGHT:
            return [
                RailDirection.TOP_RIGHT,
                RailDirection.TOP / curved_size,
                RailDirection.RIGHT / curved_size,
            ]

        else:
            raise Exception(f"Self direction ({direction.x}, {direction.y}) is invalid")

    def show(self, wait_for_input=True):

        if not self.draw:
            return None

        self.plotly.draw()

        if wait_for_input:
            self.plotly.waitforbuttonpress()

        self.plotly.pause(0.001)

        self.plotly.clf()

    def pretty_history(self):
        return [rail.__class__.__name__[0] for rail in self.rail_history]

    def is_circuit_complete(self):
        return self.start_position == self.position

# rail_list = {
#     "straight": {
#         SIZE_TINY: {
#             "count": 2
#         },
#         SIZE_SMALL: {
#             "count": 5
#         },
#         SIZE_MEDIUM: {
#             "count": 12  # one of these is a bridge
#         },
#         SIZE_LONG: {
#             "count": 6,
#             "length": 4
#         },
#         SIZE_BRIDGE_LONG: {
#             "count": 2
#         }
#     },
#     "curve": {
#         SIZE_SMALL: {
#             "count": 10
#         },
#         SIZE_MEDIUM: {
#             "count": 12
#         }
#     }
# }
